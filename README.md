# README #

This is a P5.js based version of Mine Sweeper

### Controls ###

* left click on cell to reveal its content
* right click to plant flag
* click red button (top right) to restart
