
function Cell(x, y, size, gridY){

	var pub = {
		mine:false,
		revealed:false,
		x: x * size,
		y: y * size + gridY,
		neighbours:[],
		flagged: false
	};

	var revealedColor = '#F0F0F0';
	var hiddenColor = '#D2D1D1';
	var mineColor = '#000000';
	var textColor = '#000000';
	var flagColor = '#F83030';
	var mineSize = size * 0.5;
	var half = size * 0.5;
	var textY = size * 0.75;
	var mineCount = 0;

	function init(){
		
	}

	pub.draw = function(){
		
		fill(pub.revealed ? revealedColor : hiddenColor);
		rect(pub.x, pub.y, size, size);

		if(pub.flagged){
			fill(flagColor);
			var x1 = pub.x + (size * 0.25);
			var y1 = pub.y + (size * 0.25);
			var y2 = pub.y + (size * 0.75);
			var x3 = pub.x + (size * 0.75);
			var y3 = pub.y + (size * 0.5);

			triangle(x1,y1,x1,y2,x3,y3)
			return;
		}

		if(pub.revealed && pub.mine){
			fill(mineColor);
			ellipse(pub.x + half, pub.y + half + 1, mineSize, mineSize);
		}
		
		if(pub.revealed && !pub.mine && mineCount > 0){
			fill(textColor);
			textSize(half);
			textAlign(CENTER);
			text(mineCount, pub.x + half, pub.y + textY);
		}
	}

	pub.reset = function(){
		pub.mine = false;
		pub.revealed = false;
		pub.neighbours = [];
		pub.flagged = false;
	}

	pub.containsPoint = function(x, y){

		return GeomUtils.containsPoint(x, y, pub.x, pub.y, size, size);
	}

	pub.select = function(){
		
		if(pub.flagged)
			return;

		pub.revealed = true;

		if(!pub.mine){
			mineCount = neighbouringMineCount();
		}

		if(mineCount == 0 && !pub.mine){
			showNeighbours();
		}
	}

	pub.flag = function(){

		if(pub.revealed)
			return;

		if(pub.flagged){
			//unflag
			pub.flagged = false;
		}
		else{
			pub.flagged = true;
		}
	}

	function neighbouringMineCount(){

		var count = 0;
		for(var i = 0; i < pub.neighbours.length; i++){
			if(pub.neighbours[i].mine){
				count++;
			}
		}

		return count;
	}

	function showNeighbours(){
		for(var i = 0; i < pub.neighbours.length; i++){
			var n = pub.neighbours[i];

			if(!n.revealed)
				n.select();
		}
	}

	init();
	return pub;
}