
function HUD(mineCount, grid, resetCallback){

	var pub = {
		
	}

	var hudColor = '#959393';
	var textColor = '#000000';
	var resetButtonColor = '#F83030';
	var halfX = width * 0.5;
	var halfY = gridY * 0.5;
	var textY = gridY * 0.75;
	var resetButtonSize = gridY * 0.5;
	var resetButton = {
		x: width - gridY,
		y: gridY * 0.25,
		w: resetButtonSize,
		h: resetButtonSize
	};

	function init(){

	}

	pub.draw = function(){

		fill(hudColor);
		rect(0,0,width-1, gridY);

		fill(textColor);
		textSize(halfY);
		textAlign(CENTER);

		var count = mineCount - getFlagCount();
		text(count, halfX, textY);

		//reset button
		fill(resetButtonColor);
		rect(resetButton.x, resetButton.y, resetButton.w, resetButton.h);

	}

	pub.mouseClicked = function(x, y){
		if(GeomUtils.containsPoint(x, y, resetButton.x, resetButton.y, resetButton.w, resetButton.h)){
			resetCallback();
		}
	}

	function getFlagCount(){

		var count = 0;

		for(var i = 0; i < grid.length; i++){
			for(var j = 0; j < grid[i].length; j++){
				if(grid[i][j].flagged){
					count++;
				}
			}
		}

		return count;
	}

	init();
	return pub;
}