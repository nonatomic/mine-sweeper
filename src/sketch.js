var grid;
var cellSize = 20;
var gridWidth = 0;
var gridHeight = 0;
var bombPerc = 0.3;
var mineCount = 0;
var hud;
var gridY = 40;

function setup() {

	 createCanvas(641, 481);
	 
	 gridWidth = Math.floor((width - 1)/cellSize);
	 gridHeight = Math.floor((height - 1 - gridY)/cellSize);
	 grid = makeGrid(gridWidth,gridHeight);
	 mineCount = floor((gridWidth * gridHeight) * bombPerc);
	 hud = new HUD(mineCount, grid, reset);
	 setBombs();

	 var cv = document.getElementById('defaultCanvas0');
	 cv.oncontextmenu = function (e) {
		mouseClicked();
		e.preventDefault();
	};
}

function reset(){
	console.log("RESET!!!");
	grid = makeGrid(gridWidth,gridHeight);
	setBombs();
}

function makeGrid(w, h){

	var a = [];
	for(var i = 0; i < w; i++){

		a[i] = [];
		for(var j = 0; j < h; j++){
			a[i][j] = new Cell(i, j, cellSize, gridY);
		}
	}

	//set neighbours
	for(var i = 0; i < w; i++){
		for(var j = 0; j < h; j++){
			var n = a[i][j].neighbours;

			if(j > 0) n.push(a[i][j - 1]); //n
			if(j > 0 && i < w - 1) n.push(a[i + 1][j - 1]); //ne
			if(i < w - 1) n.push(a[i + 1][j]); //e
			if(i < w - 1 && j < h - 1) n.push(a[i + 1][j + 1]); //se
			if(j < h - 1) n.push(a[i][j + 1]); //s
			if(i > 0 && j < h - 1) n.push(a[i - 1][j + 1]); //sw
			if(i > 0) n.push(a[i - 1][j]); //w
			if(i > 0 && j > 0) n.push(a[i - 1][j - 1]); //nw
		}
	}

	return a;
}

function mouseClicked(){

	for(var i = 0; i < grid.length; i++){
		for(var j = 0; j < grid[i].length; j++){
			if(grid[i][j].containsPoint(mouseX, mouseY)){
				if(mouseButton == LEFT){
					grid[i][j].select();
				}
				else{
					grid[i][j].flag();
				}
				
			}
		}
	}

	hud.mouseClicked(mouseX, mouseY);
}

function setBombs(){

	var gridClone = [];
	for(var i = 0; i < grid.length; i++){
		gridClone = gridClone.concat(grid[i]);
	}

	for(var i = 0; i < mineCount; i++){
		var rnd = floor(random(0, gridClone.length));
		gridClone[rnd].mine = true;
	 	gridClone.splice(rnd,1);
	}
}

function draw() {
   
   for(var i = 0; i < grid.length; i++){
	   for(var j = 0; j < grid[i].length; j++){
		   grid[i][j].draw();
	   }
   }

   hud.draw();
}